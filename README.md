## Übersterblichkeit: Ist Covid-19 tödlich?

Die Übersterblichkeit gibt an, ob zu einem Zeitpunkt mehr Personen verstorben sind als allgemein erwartet.
Der graue Bereich der Grafiken zeigt die Bandbreite der letzten 10 Jahre an.
Alle Grafiken wurden vom BFS [publiziert](https://www.experimental.bfs.admin.ch/expstat/de/home/innovative-methoden/momo.html) (18.08.2020).

Die Grafiken zeigen, dass es in der Schweiz seit März 2020 keine ungewöhnliche Häufung von Todesfällen gab.
Im Tessin und in der Region Genfersee gab es leicht mehr Todesfälle unter den Alten.
Die Zahlen bleiben jedoch im Bereich der Todesraten letzter Jahre.

Die Daten legen nahe: **Covid-19 führte nicht zu mehr Todesfällen in der Schweiz.**

![Mortalitätsrate Junge](/img/momo_2020-08-11.png)

## Altersverteilung: Betrifft Covid-19 alle?

Das BAG [publiziert](https://covid-19-schweiz.bagapps.ch/de-1.html) Daten zu «Covid-Toten» (18.08.2020).
Dies sind Verstorbene, bei welchen vor oder nach dem Tod der Test auf Covid-19 positiv ausfiel.
Covid-19 kann dabei nicht zwingend als Todesursache gelten, die Mehrheit der Verstorbenen hatte bereits andere schwerwiegende Erkrankungen.
Das Alter der Betroffenen deckt sich mit der durchschnittlichen Lebenserwartung in der Schweiz.

Die Daten legen nahe: **Covid-19 führt nicht zu vorzeitig eintretenden Todesfällen.**

![Altersverteilung](/img/todesfaelle_altersklassen_2020-08-17.png)

## Aber die Infektionszahlen steigen!

Neben der Anzahl positiven Tests steigt auch die Anzahl der negativ getesteten Personen.
Die Anzahl der «Toten» steigt hingegen nicht an.
Die meisten Schweizer haben ein Immunsystem, welches mit Covid-19 gut fertig wird.
Die Mehrheit hat keine oder milde Symptome.
«Hohe Infektionszahlen» mit gleichbleibender tiefer Sterberate sind eine gute Nachricht:

**Covid-19 hat keine hohe Letalität**

## Lockdown sei Dank?

Ist die Schweiz nur dank dem «Lockdown» so gut davon gekommen?
Dies lässt sich nicht belegen.
Folgende Argumente sprechen dagegen:

 * Die Massnahmen kamen oft «zu spät».
 * Auf den Statistiken haben die «Massnahmen» keinen «Knick» hinterlassen.
 * Jede Grippewelle stieg bisher exponentiell an und flachte danach wieder ab.
 * Die Korrektheit der Testverfahren ([PCR-Test](https://www.bag.admin.ch/dam/bag/de/dokumente/mt/k-und-i/aktuelle-ausbrueche-pandemien/2019-nCoV/merkblatt-swissmedic-covid-19-testung.pdf.download.pdf/Merkblatt_zur_aktuellen_COVID-19_Testung_in_der_Schweiz_Swissmedic_BAG.pdf)) und Nützlichkeit der Massnahmen (z.B. [Maskenpflicht](https://www.bag.admin.ch/dam/bag/de/dokumente/cc/kom/covid-19-vollzugsmonitoring-exec-summ-bericht5.pdf.download.pdf/Vollzugsmonitoring_COVID-19_Executive_Summary_Bericht-5.pdf)) sind auch gemäss Behörden nicht unbestritten (21.08.2020).

Die Beweislast läge auf Seiten der Lockdown-Befürworter:
**Bisher gibt es keinen stichhaltigen Nachweis, dass es eine Epidemie in der Schweiz gibt und der Lockdown angemessen davor schützt.**

## Warum diese Überreaktion?

Die Einschnitte in die Rechte der Bürger und die Berichterstattung der staatlichen Informationskanäle grenzt an Angstmacherei.
Sie lässt sich spätestens seit diesem Sommer nicht mehr rechtfertigen.
Warum wird so weitergefahren?

Diese Frage lässt sich aufgrund der öffentlichen Daten nicht beantworten und bleibt hier ungeklärt.

## Was kann ich machen?

Prüfen und Verteilen Sie diese Information:

 * https://gitlab.com/gottvertelli/situation-schweiz

Beteiligen Sie sich an der demokratischen Aufarbeitung in der Schweiz:

 * https://notrecht-referendum.ch
 * https://fruehling2020.com

**Sprechen Sie mit Ihren Mitmenschen darüber!**

# Covid-19 Pandemie - auch in der Schweiz?

Der Bundesrat hat seit dem Frühling 2020 einschneidende Eingriffe in die Freiheit der Schweizer Bevölkerungen vorgenommen.
Dies hat er mit einer Epidemie begründet.
Spätestens seit diesem Sommer zeigen offizielle Daten, dass es in der Schweiz nie eine Epidemie gab.
Dieses Dokument möchte die Schweizer Bevölkerung auf diesen Umstand aufmerksam machen.
Sämtliche Daten stammen von den Bundesämtern für Statistik (BFS) und für Gesundheit (BAG).
Die entsprechenden Quellen sind jeweils angegeben.
